Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and to execute the file
[installBeck2019a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/beck2019a/raw/master/installBeck2019a.sh)
in this directory.

```bash
mkdir -p Beck2019a && cd Beck2019a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/beck2019a/raw/master/installBeck2019a.sh
chmod u+x installBeck2019a.sh
bash ./installBeck2019a.sh
```

Two modules for the fully-coupled and the decoupled version of the code will be created.
Within each, there are three test cases for the homogeneous single phase scenario, the CO2 scenario and for the efficiency test:

  test/geomechanics/el2p/el2pdecoupled_singlephase.cc
  test/geomechanics/el2p/el2pdecoupled_co2.cc
  test/geomechanics/el2p/el2pdecoupled_efficiencytest.cc

and

  test/geomechanics/el2p/el2p_singlephase.cc,
  test/geomechanics/el2p/el2p_co2.cc,
  test/geomechanics/el2p/el2p_efficiencytest.cc

You can build the module just like any other DUNE
module. For building and running the executables, please go to the folders
containing the sources listed above.


